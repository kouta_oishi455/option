﻿using UnityEngine;
using System.Collections.Generic;

public class OptionMoveControll : MonoBehaviour{

    public GameObject optionObj;
    int interval = 30;

    List<Vector3> playerPotion = new List<Vector3>();
    void Start()
    {
        playerPotion.Add(transform.localPosition);
    }
    void Update()
    {
        if (playerPotion[playerPotion.Count - 1] != this.transform.localPosition)
        {
            playerPotion.Add(transform.localPosition);

            if (playerPotion.Count > interval)
            {
                optionObj.transform.localPosition = playerPotion[0];
                playerPotion.RemoveAt(0);
            }
        }
    }
}